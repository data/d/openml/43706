# OpenML dataset: Uber-and-Lyft-Dataset-Boston-MA

https://www.openml.org/d/43706

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Uber vs Lyft
This is a very beginner-friendly dataset. It does contain a lot of NA values. It is a good dataset if you want to use a Linear Regression Model to see the pattern between different predectors such as hour and price.
A really amazing part of this dataset is that I have included the corresponding weather data for that hour with a short summary of the weather. Other important factors are temperature, wind, and sunset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43706) of an [OpenML dataset](https://www.openml.org/d/43706). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43706/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43706/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43706/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

